import React from 'react';
import './App.css';
import AddForm from './Components/AddForm'
import List from './Components/List'

function App() {
  return (
    <div className="wrapper">
      <AddForm></AddForm>
      <List></List>
    </div>
  );
}

export default App;

import {TYPES} from './types'

export function addItem(payload:any) {
    return {
        type: TYPES.ADD_NEW_ITEM,
        payload,
    }
}

export function complitedItem(payload:any) {
    return {
        type: TYPES.COMPLITED_ITEM,
        payload
    }
}

export function removeItem(payload:any) {
    return {
        type: TYPES.REMOVE_ITEM,
        payload,
    }
}


import { combineReducers } from "redux";
import  Reducer from './reducer'

export default () => 
    combineReducers( {
        MAIN: Reducer,
});
import { TYPES } from './types'
import {todosStart} from '../Components/todosStart'

const initialState = {
    todos: todosStart
}

    export default function Reducer(state:any = initialState, action:any) {

    switch(action.type) {
        case TYPES.ADD_NEW_ITEM: 
            if(action.payload.title) {
                return { ...state, todos: [...state.todos, action.payload] }
            } else {
                return state
            }

        case TYPES.COMPLITED_ITEM:
            console.log('reducer COMPLITED_ITEM')
            console.log('payload: ', action.payload)

            const compiledTodos = state.todos.map((item:any) => {
                if(item.id === action.payload.id) {
                    item.complited = !item.complited
                }
                return item
            })
            return {...state, todos: compiledTodos }


        case TYPES.REMOVE_ITEM:
            const newTodos = state.todos.filter( (item:any) => {
                return item.id !== action.payload
            })
            const changedTodos = newTodos.map( (item:any, index:number) => {
                item.id = index+1
                return item
            })
            return { ...state, todos: changedTodos}
            
        default: 
            return state 
    }
} 
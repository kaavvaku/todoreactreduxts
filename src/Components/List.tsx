import React from 'react'
import { useSelector } from 'react-redux'
import ListItem from './ListItem'
import { getStateMain } from './../Redux/selectors'

export default function List() {

    const todos:any = useSelector(getStateMain)

    return (
        <ul className="ul">
            {
                todos.map( (item:any, index:number) => {
                    return <ListItem id={index+1} title={item.title} complited={item.complited} key={index} ></ListItem>
                })
            }
        </ul>
    )
}
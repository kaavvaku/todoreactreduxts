import { useDispatch,  } from 'react-redux';
import { complitedItem, removeItem } from '../Redux/actions'

export default function ListItem(todoItem:any) {
    const classes:string[] = [];
    const dispatch:any = useDispatch()

    if(todoItem.complited) { classes.push('done') }

    return (
        <li className="li">
        <span className={classes.join('')}>
            <input className="checkbox" type="checkbox" onChange={()=>dispatch(complitedItem(todoItem))} checked={todoItem.complited}></input>
            <span style={{marginRight: '1rem'}}>{todoItem.id + '.'}</span>
            {todoItem.title}
        </span>
        <button onClick={()=>dispatch(removeItem(todoItem.id))}>&times;</button>
    </li>
    )
}

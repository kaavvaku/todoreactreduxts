import React from 'react'
import { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { addItem } from './../Redux/actions'
import { getStateMain } from './../Redux/selectors'

export default function AddForm() {

    const dispatch:any = useDispatch()
    const state_add = useSelector(getStateMain)
    const [value, setValue] = useState('')

    const payload:any = {
        id: state_add.length+1,
        compiled: false, 
        title: value
    }

    function handlerOnSubmit(event:any) {
        event.preventDefault();
        if(value.trim()) { setValue('') }
    }

    return (
        <div>
             <h1>REDUX HELL</h1>
             <form className="form" onSubmit={handlerOnSubmit} >
                 <input className="input" placeholder="Please, ..."  value={value} onChange={event => setValue(event.target.value)}></input>
                 <button className="button" type="submit"  onClick={()=>dispatch(addItem(payload))}>Добавить</button>
             </form>
        </div>
    )     
}

